#include <dbus/dbus.h>
#include <unistd.h>
#include <signal.h>
#include <string.h>
#include <syslog.h>
#include "daemon.h"
#include "sleep.h"

static volatile sig_atomic_t terminate = 0;

struct wakeupd {
	DBusConnection *conn;
	int lockfd;
};

static void handle_SIGTERM(int num)
{
	sleep_leave();
	_exit(0);
}

static int handle_dbus_error(DBusError *err, const char *description, int crit)
{
	if(dbus_error_is_set(err)){
		syslog(crit ? LOG_CRIT : LOG_ERR, "%s: %s",
			description, err->message);
		dbus_error_free(err);
		return 1;
	}
	return 0;
}

static void method_reply_empty(DBusConnection *conn, DBusMessage *call)
{
	DBusMessage *reply;

	reply = dbus_message_new_method_return(call);

	if(!dbus_connection_send(conn, reply, NULL)){
		syslog(LOG_CRIT, "Could not reply, out of memory");
	}
	
	dbus_message_unref(reply);
}

static int dbus_init(struct wakeupd *wakeupd)
{
	DBusError err;
	int ret;
	dbus_error_init(&err);

	wakeupd->conn = dbus_bus_get(DBUS_BUS_SYSTEM, &err);
	handle_dbus_error(&err, "Connection Error", 1);
	if(!wakeupd->conn) return 1;
	syslog(LOG_DEBUG, "Got system bus");

	ret = dbus_bus_request_name(wakeupd->conn, "bananian.WakeupManager",
			DBUS_NAME_FLAG_REPLACE_EXISTING |
			DBUS_NAME_FLAG_DO_NOT_QUEUE, &err);
	if(handle_dbus_error(&err, "Could not assign name", 1)){
		return 1;
	}
	if(ret != DBUS_REQUEST_NAME_REPLY_PRIMARY_OWNER){
		syslog(LOG_ERR, "Could not assign name, already owned");
		return 1;
	}

	dbus_bus_add_match(wakeupd->conn,
		"type='signal',interface='org.freedesktop.login1.Manager'",
		&err);
	handle_dbus_error(&err, "Failed to set match rule for suspend", 0);
	dbus_bus_add_match(wakeupd->conn,
		"type='signal',"
		"interface='org.freedesktop.DBus.Properties',"
		"path='/org/freedesktop/login1'", &err);
	handle_dbus_error(&err, "Failed to set match rule for idle", 0);
	return 0;
}

static void suspend_lock(struct wakeupd *wakeupd)
{
	DBusError err;
	DBusMessage *msg, *reply;
	const char *lock_what = "sleep";
	const char *lock_who = "Bananian Sleep/Wakeup Manager";
	const char *lock_why = "Ready to enable autosleep";
	const char *lock_mode = "delay";
	dbus_error_init(&err);

	msg = dbus_message_new_method_call(
			"org.freedesktop.login1",
			"/org/freedesktop/login1",
			"org.freedesktop.login1.Manager",
			"Inhibit");
	if(!msg){
		syslog(LOG_ERR, "Failed to create call to Inhibit");
		return;
	}

	if(!dbus_message_append_args(msg,
			DBUS_TYPE_STRING, &lock_what,
			DBUS_TYPE_STRING, &lock_who,
			DBUS_TYPE_STRING, &lock_why,
			DBUS_TYPE_STRING, &lock_mode,
			DBUS_TYPE_INVALID))
	{
		dbus_message_unref(msg);
		syslog(LOG_ERR, "Failed to set arguments for call to Inhibit");
		return;
	}

	reply = dbus_connection_send_with_reply_and_block(
			wakeupd->conn, msg, -1, &err);
	dbus_message_unref(msg);
	if(!reply){
		handle_dbus_error(&err,
			"Failed to get suspend inhibitor lock", 0);
		return;
	}

	if(!dbus_message_get_args(reply, &err,
		DBUS_TYPE_UNIX_FD, &wakeupd->lockfd,
		DBUS_TYPE_INVALID))
	{
		handle_dbus_error(&err,
			"Failed to get suspend inhibitor lock FD", 0);
	}
	dbus_message_unref(reply);
}

static void suspend_unlock(struct wakeupd *wakeupd)
{
	close(wakeupd->lockfd);
	wakeupd->lockfd = -1;
}

static void lock_or_sleep(struct wakeupd *wakeupd, DBusMessage *msg)
{
	DBusError err;
	dbus_bool_t active = TRUE;
	if(!dbus_message_get_args(msg, &err,
		DBUS_TYPE_BOOLEAN, &active,
		DBUS_TYPE_INVALID))
	{
		handle_dbus_error(&err,
			"Failed to get suspend status", 0);
		return;
	}
	if(active){
		sleep_enter();
		suspend_unlock(wakeupd);
	}
	else {
		suspend_lock(wakeupd);
	}
}

static void handle_property(DBusMessageIter *dict)
{
	DBusMessageIter val;
	char *propname;

	if(dbus_message_iter_get_arg_type(dict) != DBUS_TYPE_STRING){
		syslog(LOG_ERR, "Property name not string");
		return;
	}
	dbus_message_iter_get_basic(dict, &propname);
	if(0 == strcmp(propname, "IdleHint")){
		dbus_bool_t idle = FALSE;
		if(!dbus_message_iter_next(dict)){
			syslog(LOG_ERR, "No value");
			return;
		}
		if(dbus_message_iter_get_arg_type(dict) !=
			DBUS_TYPE_VARIANT)
		{
			syslog(LOG_ERR, "Value is not a variant");
			return;
		}
		dbus_message_iter_recurse(dict, &val);
		if(dbus_message_iter_get_arg_type(&val) !=
			DBUS_TYPE_BOOLEAN)
		{
			syslog(LOG_ERR, "Idle property not boolean");
			return;
		}
		dbus_message_iter_get_basic(&val, &idle);
		if(idle){
			sleep_enter();
		}
		else {
			sleep_leave();
		}
	}
}

static void handle_properties_changed(struct wakeupd *wakeupd, DBusMessage *msg)
{
	DBusMessageIter iter, dict, pair;
	dbus_message_iter_init(msg, &iter);
	if(dbus_message_iter_get_arg_type(&iter) != DBUS_TYPE_STRING){
		syslog(LOG_ERR, "Property interface is not a string");
	}
	if(!dbus_message_iter_next(&iter)){
		syslog(LOG_ERR, "No properties!");
	}
	if(dbus_message_iter_get_arg_type(&iter) !=
		DBUS_TYPE_ARRAY ||
		dbus_message_iter_get_element_type(&iter) !=
		DBUS_TYPE_DICT_ENTRY)
	{
		syslog(LOG_ERR, "Properties are not a dictionary");
		return;
	}
	dbus_message_iter_recurse(&iter, &dict);
	do {
		dbus_message_iter_recurse(&dict, &pair);
		handle_property(&pair);
	} while(dbus_message_iter_next(&dict));
}

void mainloop(void)
{
	struct wakeupd wakeupd;
	struct sigaction sa;

	wakeupd.lockfd = -1;

	sa.sa_flags = 0;
	sa.sa_handler = handle_SIGTERM;
	sigemptyset(&sa.sa_mask);
	sigaction(SIGTERM, &sa, NULL);

	if(dbus_init(&wakeupd)) return;

	suspend_lock(&wakeupd);

	while(1){
		DBusMessage *msg;

		dbus_connection_read_write_dispatch(wakeupd.conn, -1);
		msg = dbus_connection_pop_message(wakeupd.conn);
		if(!msg) continue;

		if(dbus_message_is_method_call(msg,
				"bananian.WakeupManager",
				"WakeUp"))
		{
			sleep_leave();
			method_reply_empty(wakeupd.conn, msg);
		}
		else if(dbus_message_is_method_call(msg,
				"bananian.WakeupManager",
				"Sleep"))
		{
			sleep_enter();
			method_reply_empty(wakeupd.conn, msg);
		}
		else if(dbus_message_is_signal(msg,
				"org.freedesktop.login1.Manager",
				"PrepareForSleep"))
		{
			lock_or_sleep(&wakeupd, msg);
		}
		else if(dbus_message_is_signal(msg,
				"org.freedesktop.DBus.Properties",
				"PropertiesChanged"))
		{
			handle_properties_changed(&wakeupd, msg);
		}

		dbus_message_unref(msg);
	}
}
