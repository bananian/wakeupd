#include <errno.h>
#include <string.h>
#include <syslog.h>
#include <stdio.h>
#include "sleep.h"

#define AUTOSLEEP_FILE "/sys/power/autosleep"
#define WAKE_UNLOCK_FILE "/sys/power/wake_unlock"
#define WAKE_LOCK_FILE "/sys/power/wake_lock"

void sleep_leave(void)
{
	FILE *f;

	f = fopen(WAKE_LOCK_FILE, "w");
	if(!f){
		syslog(LOG_ERR, "Could not wake up: open %s: %s",
			WAKE_LOCK_FILE, strerror(errno));
		return;
	}
	if(EOF == fputs("wakeupmanager\n", f)){
		syslog(LOG_ERR, "Could not wake up: write to %s: %s",
			WAKE_LOCK_FILE, strerror(errno));
	}
	fclose(f);
}

void sleep_enter(void)
{
	FILE *f;

	f = fopen(WAKE_UNLOCK_FILE, "w");
	if(!f){
		syslog(LOG_ERR, "Could not fall asleep: open %s: %s",
			WAKE_UNLOCK_FILE, strerror(errno));
		return;
	}
	if(EOF == fputs("wakeupmanager\n", f)){
		syslog(LOG_ERR, "Could not fall asleep: write to %s: %s",
			WAKE_UNLOCK_FILE, strerror(errno));
	}
	fclose(f);
}

void enable_autosleep(void)
{
	FILE *f;

	f = fopen(AUTOSLEEP_FILE, "w");
	if(!f){
		syslog(LOG_ERR, "Could not fall asleep: open %s: %s",
			AUTOSLEEP_FILE, strerror(errno));
		return;
	}
	if(EOF == fputs("mem\n", f)){
		syslog(LOG_ERR, "Could not fall asleep: write mem to %s: %s",
			AUTOSLEEP_FILE, strerror(errno));
	}
	fclose(f);
}
