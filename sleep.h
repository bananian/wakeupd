#ifndef _WAKEUPD_SLEEP_H_
#define _WAKEUPD_SLEEP_H_

void enable_autosleep(void);
void sleep_leave(void);
void sleep_enter(void);

#endif
