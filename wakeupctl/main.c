#include <string.h>
#include <stdio.h>
#include <dbus/dbus.h>

static void usage(const char *argv0, const char *reason){
	fprintf(stderr, "%s\nUsage: %s sleep|wakeup\n", reason, argv0);
}

int main(int argc, char * const *argv)
{
	DBusError err;
	DBusConnection *connection;
	DBusMessage *msg;
	const char *method;

	if(argc != 2){
		usage(argv[0], "Expected an argument.");
		return 1;
	}
	if(0 == strcasecmp(argv[1], "sleep")){
		method = "Sleep";
	}
	else if(0 == strcasecmp(argv[1], "wakeup")){
		method = "WakeUp";
	}
	else {
		usage(argv[0], "Expected either sleep or wakeup.");
		return 1;
	}
	dbus_error_init(&err);
	connection = dbus_bus_get(DBUS_BUS_SYSTEM, &err);
	if(dbus_error_is_set(&err)){
		fprintf(stderr, "Failed to connect to system bus: %s\n",
			err.message);
		dbus_error_free(&err);
		return 2;
	}
	msg = dbus_message_new_method_call(
			"bananian.WakeupManager",
			"/",
			"bananian.WakeupManager",
			method);
	if(!msg){
		fprintf(stderr, "Couldn't create call to %s\n", method);
		return 2;
	}
	dbus_connection_send_with_reply_and_block(
			connection, msg, -1, &err);
	if(dbus_error_is_set(&err)){
		fprintf(stderr, "Failed to call %s: %s\n",
				method, err.message);
		return 2;
	}
	return 0;
}
